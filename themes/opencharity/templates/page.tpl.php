<header>
    <nav>
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="logo">
                        <?php if ($logo): ?>
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"
                               id="logo">
                                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-9">
                    <div class="navigation-menu">
                        <ul class="pull-right">
                            <li>
                                <a href="">
                                    about open charity
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    the blog
                                </a>
                            </li>
                            <li>
                                <a href="" class="btn-outline">
                                    join us
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </nav>
</header>

<!--Site Cover Image-->
<section class="s-cover">
    <div class="bg-overlay"></div>
    <article>
        <h1 class="tag-line">Sharing Ideas For Charities</h1>

        <p class="para para-1">Many charities’ goals are similar, as is the functionality we require, but little shared
            working takes place.</p>

        <p class="para para-2">By working together, driving shared areas of interest and influencing open source
            developments we can bring efficiencies, improve the digital experience for our users, and have great
            impact.</p>

        <p class="para para-3">Together we can make a bigger difference.</p>

    </article>
</section>

<!--News Section-->

<section class="s-news">
    <article class="container">
        <div class="row">
            <div class="col-8">
                <p class="s-news-dt"><span class="s-news-label">Next Event:</span> June 23rd 2016 <span class="fw-600">18:30 - 21:00</span>
                </p>

                <p class="s-news-venue">Cancer Research UK, Angel Building, 407 St John Street, London EC1V 4AD</p>
            </div>
            <div class="col-4 s-news-register">
                <a href="" class="btn-register pull-right">register</a>
            </div>
        </div>
    </article>
</section>

<!-- Get Involved Section-->

<section class="s-common s-involve">
    <article class="container">
        <h3 class="s-title">Get Involved</h3>

        <div class="row">
            <div class="col-4">
                <article>
                    <div class="g-img mb10">
                        <?php print '<img src="' . $base_path . path_to_theme() . '/images/meetup.png' . '" alt="meetup" />' ?>
                    </div>
                    <div class="g-content">
                        <div class="g-heading fs-16 mb20 fw-500">
                            We do meetings
                        </div>
                        <div class="g-description fs-14 mb20">
                            We organise our meetings through the OpenCharity MeetUp group
                        </div>
                        <div class="g-btn">
                            <a href="" class="g-btn-link">meetup group</a>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-4">
                <article>
                    <div class="g-img mb10">
                        <?php print '<img src="' . $base_path . path_to_theme() . '/images/slack.png' . '" alt="meetup" />' ?>
                    </div>
                    <div class="g-content">
                        <div class="g-heading fs-16 mb20 fw-500">
                            We Collaborate
                        </div>
                        <div class="g-description fs-14 mb20">
                            OpenCharity have a slack group for
                            daily collaboration
                        </div>
                        <div class="g-btn">
                            <a href="" class="g-btn-link">slack group</a>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-4">
                <article>
                    <div class="g-img mb10">
                        <?php print '<img src="' . $base_path . path_to_theme() . '/images/screen.png' . '" alt="meetup" />' ?>
                    </div>
                    <div class="g-content">
                        <div class="g-heading fs-16 mb20 fw-500">
                            we share
                        </div>
                        <div class="g-description fs-14 mb20">
                            We have a Google Group set up to share tools
                            and documents
                        </div>
                        <div class="g-btn">
                            <a href="" class="g-btn-link">google group</a>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </article>
</section>

<!-- Our Mission Section-->

<section class="s-common s-mission">
    <article class="container">
        <h3 class="s-title">Our mission</h3>

        <p class="s-para fs-14 mb20">Charities and Partners collaborating and sharing open solutions and ideas to create
            value in the digital space.</p>

        <p class="s-para fs-16 fw-500">If you are a charity or a supplier, we are ready to welcome you.</p>

        <div class="row">
            <div class="col-4">
                <article>
                    <div class="g-img mb10">
                        <?php print '<img src="' . $base_path . path_to_theme() . '/images/bulb.png' . '" alt="meetup" />' ?>
                    </div>
                    <div class="g-content">
                        <div class="g-heading fs-16 mb20 fw-500">
                            We help charities
                        </div>
                        <div class="g-description fs-14 mb20">
                            share knowledge and working practice to make the best technology choices.
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-4">
                <article>
                    <div class="g-img mb10">
                        <?php print '<img src="' . $base_path . path_to_theme() . '/images/people.png' . '" alt="meetup" />' ?>
                    </div>
                    <div class="g-content">
                        <div class="g-heading fs-16 mb20 fw-500">
                            We bring together
                        </div>
                        <div class="g-description fs-14 mb20">
                            charities and suppliers to the charity sector to share best practices.
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-4">
                <article>
                    <div class="g-img mb10">
                        <?php print '<img src="' . $base_path . path_to_theme() . '/images/like.png' . '" alt="meetup" />' ?>
                    </div>
                    <div class="g-content">
                        <div class="g-heading fs-16 mb20 fw-500">
                            We encourage
                        </div>
                        <div class="g-description fs-14 mb20">
                            collaboration and innovation for the good of the charity sector.
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </article>
    <div class="container separator"></div>
    <article class="container">
        <h3 class="s-title">Our members</h3>

        <div class="m-slider">
            <input type="radio" id="m-slide1" name="m-slide" checked>
            <input type="radio" id="m-slide2" name="m-slide">
            <input type="radio" id="m-slide3" name="m-slide">

            <div class="m-slides">
                <div class="overflow">
                    <div class="inner">
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p5.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p4.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p3.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p2.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p1.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p6.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                        <article>
                            <a href="#">
                                <?php print '<img src="' . $base_path . path_to_theme() . '/images/p7.png' . '" alt="cancer research uk" />' ?>
                            </a>
                        </article>
                    </div>
                </div>
            </div>
            <div class="m-slide-indicator mt20">
                <label for="m-slide1"></label>
                <label for="m-slide2"></label>
                <label for="m-slide3"></label>
            </div>
        </div>
    </article>
</section>

<!-- Blog Section-->

<section class="s-common s-blog">
    <article class="container">
        <h3 class="s-title">Blog</h3>

        <div class="b-slider">
            <input type="radio" id="b-slide1" name="b-slide" checked>
            <input type="radio" id="b-slide2" name="b-slide">
            <input type="radio" id="b-slide3" name="b-slide">

            <div class="b-slides">
                <div class="overflow">
                    <div class="inner">
                        <article>
                            <h3 class="b-title fs-14 fw-600">Online Donations Special consectetur adipiscing</h3>

                            <div class="separator"></div>
                            <p class="b-para fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                            <div class="separator"></div>
                            <div class="b-date fs-14">24 Nov 2017</div>
                        </article>
                        <article>
                            <h3 class="b-title fs-14 fw-600">Online Donations Special consectetur adipiscing</h3>

                            <div class="separator"></div>
                            <p class="b-para fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                            <div class="separator"></div>
                            <div class="b-date fs-14">24 Nov 2017</div>
                        </article>
                        <article>
                            <h3 class="b-title fs-14 fw-600">Online Donations Special consectetur adipiscing</h3>

                            <div class="separator"></div>
                            <p class="b-para fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                            <div class="separator"></div>
                            <div class="b-date fs-14">24 Nov 2017</div>
                        </article>
                        <article>
                            <h3 class="b-title fs-14 fw-600">Online Donations Special consectetur adipiscing</h3>

                            <div class="separator"></div>
                            <p class="b-para fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                            <div class="separator"></div>
                            <div class="b-date fs-14">24 Nov 2017</div>
                        </article>
                        <article>
                            <h3 class="b-title fs-14 fw-600">Online Donations Special consectetur adipiscing</h3>

                            <div class="separator"></div>
                            <p class="b-para fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                            <div class="separator"></div>
                            <div class="b-date fs-14">24 Nov 2017</div>
                        </article>
                        <article>
                            <h3 class="b-title fs-14 fw-600">Online Donations Special consectetur adipiscing</h3>

                            <div class="separator"></div>
                            <p class="b-para fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                            <div class="separator"></div>
                            <div class="b-date fs-14">24 Nov 2017</div>
                        </article>

                    </div>
                </div>
            </div>
            <div class="b-slide-control mt20">
                <label for="b-slide1"></label>
                <label for="b-slide2"></label>
                <label for="b-slide3"></label>

            </div>
        </div>
    </article>
</section>

<!-- Footer Section-->
<footer class="footer">
    <article class="container">
        <div class="row">
            <div class="col-12">
                <div class="s-media-links">
                    <span class="fa fa-facebook-f"></span>
                    <span class="fa fa-twitter"></span>
                    <span class="fa fa-google-plus"></span>
                </div>
                <div class="separator"></div>
                <p class="disclaimer">This site was built as a collaboration between Manifesto Digital and Compucorp</p>

            </div>
        </div>
    </article>

</footer>

